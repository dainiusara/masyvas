﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace masyvas1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[3];

            Console.WriteLine("Iveskite 3 skaicius");

            //numbers[0] = 2;
            //numbers[1] = 4;
            //numbers[2] = 6;
            //numbers[3] = 8;
            //numbers[4] = 10;


            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = Convert.ToInt32(Console.ReadLine());
            }
            int suma = 0;
            int didziausias = numbers.Max();
            int maziausias = numbers.Min();
            for (int i = 0; i < numbers.Length; i++)       
            {
                suma = suma + numbers[i];
            }

            int pilnameciai = 0;
            for (int i = 0; i < numbers.Length; i++)
             if (numbers[i] >= 18)
            {
                    pilnameciai++;
            }
            Console.WriteLine("Pilnameciai " + pilnameciai);
            Console.WriteLine("Didziausias skaicius: " + didziausias);
            Console.WriteLine("Maziausias skaicius: " + maziausias);
            Console.WriteLine("Suma: " + suma);
            Console.ReadLine();
        }
    }
}
